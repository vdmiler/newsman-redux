import axios from 'axios';
import {FETCH_NEWS_ERROR, FETCH_NEWS_SUCCESS, FETCH_NEWS_START} from './actionTypes';

export function fetchNews() {
    return async dispatch => {
        dispatch(fetchNewsStart());
        try {
            const response = await axios.get('https://newsapi.org/v2/top-headlines?country=us', {
                headers: {
                    Authorization: 'Bearer c6845b90dfa847d1b4b5c25cc4923285'
                }
            })
            dispatch(fetchNewsData(response.data))
        } catch (e) {
            dispatch(fetchNewsError(e))
        }
    }
}

function fetchNewsStart() {
    return {
        type: FETCH_NEWS_START
    }
} 

function fetchNewsData(news) {
    return {
        type: FETCH_NEWS_SUCCESS,
        news: news.articles
    }
} 

function fetchNewsError(e) {
    return {
        type: FETCH_NEWS_ERROR,
        error: e
    }
} 