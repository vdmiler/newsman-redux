import React from 'react';
import '../../App.scss';

const NewsDetail = props => {
    // check obj
    let obj = null;
    if(props.location.state != undefined) {
        obj = props.location.state[0]
    }
    // prepare title
    let str = obj.title;
    let strArr = str.split(' ');
    let newStr = strArr.splice(0, strArr.length - 1).join(' ');
    // prepare date
    const today = new Date(obj.publishedAt);
    const getZero = date => date < 10 ? '0' + date : date;
    const month = getZero(today.getMonth() + 1);
    const day = getZero(today.getDate());
    return (
        <div className="news-detail">
            <div className="news-detail__left">
                <h1 className="news-detail__title">
                    {newStr} <span>{strArr[strArr.length - 1]}</span>
                </h1>
                <a href={obj.url} className="news-detail__link">
                    {obj.source.name}
                </a>

                <div className="news-detail__date">
                    <span>{month}</span>
                    {day}
                </div>
            </div>
            <div className="news-detail__right">
                <div className="news-detail__picture">
                    <img src={obj.urlToImage} alt={obj.title} className="news-detail__img"/>
                </div>
                <p className="news-detail__text">
                    {obj.content}
                </p>
            </div>
        </div>
    )
}

export default NewsDetail;