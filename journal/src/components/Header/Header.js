import React from 'react';
import {NavLink} from 'react-router-dom';
import '../../App.scss';

const Header = () => {
    return (
        <header className="header" id="header">
            <div className="container">
                <div className="hero">
                    <NavLink exact to="/" className="logo">
                        Новостник
                    </NavLink>
                    <nav className="menu">
                        <ul className="menu__list">
                            <li className="menu__item">
                                <NavLink exact to="/" className="menu__link">
                                    Главная
                                </NavLink>
                            </li>
                            <li className="menu__item">
                                <NavLink exact to="/News" className="menu__link">
                                    Новости
                                </NavLink>
                            </li>
                            <li className="menu__item">
                                <NavLink exact to="/Contacts" className="menu__link">
                                    Контакты
                                </NavLink>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
    )
}

export default Header;
