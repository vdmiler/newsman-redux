import {FETCH_NEWS_ERROR, FETCH_NEWS_SUCCESS, FETCH_NEWS_START} from "../actions/actionTypes";

const initialState = {
    news: null,
    loading: false,
    error: null
}

export default function newsReducer(state = initialState, action) {
    switch(action.type) {
        case FETCH_NEWS_START:
            return {
                ...state, loading: true, news: action.news
            }
        case FETCH_NEWS_SUCCESS:
            return {
                ...state, loading: false, news: action.news
            }
        case FETCH_NEWS_ERROR:
            return {
                ...state, loading: false, error: action.error
            }
        default:
            return state;
    }
}