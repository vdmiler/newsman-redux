import React from 'react';
import './Loader.css';

const Loader = props => {
    return (
        <div className={`loader-wrapper ${props.addClass ? 'plus' : 'minus'}`}>
            <div className="loader">
                <div/><div/>
            </div>
        </div>
    )
}

export default Loader;