import React from 'react';
import './App.scss';
import {Route} from 'react-router-dom';
import Header from './components/Header/Header';
import Main from './components/Main/Main';
import NewsDetail from './components/NewsDetail/NewsDetail';
import Contacts from './components/Contacts/Contacts';
import Footer from './components/Footer/Footer';

const App = () => {
    return (
        <div className="App">
            <Header/>
            <div className="content">
                <div className="container">
                    <Route
                        path="/"
                        exact
                        component={Main}
                    />
                    <Route
                        path="/News"
                        exact
                        component={Main}
                    />
                    <Route
                        path="/Contacts"
                        exact
                        component={Contacts}
                    />
                    <Route
                        path="/News/:name"
                        exact
                        component={NewsDetail}
                    />
                </div>
            </div>
            <Footer/>
        </div>
    );
}

export default App;
