import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import '../../App.scss';
import TitleMain from '../TitleMain/TitleMain';
import TitleNews from '../TitleNews/TitleNews';
import NewsItem from '../NewsItem/NewsItem';
import NewsLink from '../NewsLink/NewsLink';
import {connect} from 'react-redux';
import {fetchNews} from '../../store/actions/news';
import Loader from '../UI/Loader/Loader';

class Main extends Component {
    componentDidMount() {
        this.props.fetchNews()
    }
    render() {
        const resultApi = this.props.news;
        let itemsArr = null;
        const filterArr = resultApi && resultApi.filter((item, index) => {
            return index < 6;
        })
        this.props.location.pathname == '/' ? itemsArr = filterArr : itemsArr = resultApi;      
        return (
            <div className="news">
                {
                this.props.location.pathname == '/' ? <TitleMain/> : 
                this.props.location.pathname == '/News' ? <TitleNews/> :
                null
                }
                <div className="news__list">
                    {   
                        this.props.loading
                        ?
                        <Loader
                            addClass={this.props.location.pathname == '/'}
                        />
                        :
                        itemsArr && itemsArr.map((item, index) => {
                            return (
                                <NewsItem
                                    key={index}
                                    title={item.title}
                                    url={item.url}
                                    source={item.source.name}
                                    date={item.publishedAt}
                                    obj={item}
                                />
                            )
                        })
                    }
                </div>
                {this.props.location.pathname == '/' ? <NewsLink/> : null}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        news: state.news.news,
        loading: state.news.loading
    }
}

function mapDispatchToProps(dispatch) {
    return {
        fetchNews: () => dispatch(fetchNews())
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (withRouter(Main));